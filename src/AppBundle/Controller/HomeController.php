<?php

namespace AppBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\ArrayType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function homeAction()
    {
        // replace this example code with whatever you need
        $em = $this->getDoctrine()->getEntityManager();
        $categories = $em->getRepository('AppBundle:Category')->findAll();
        $pubsByCategory = new ArrayCollection();
        $pubRepo = $em->getRepository('AppBundle:Publication');
        foreach ($categories as $category) {
            $pubsByCategory->add(array(
                'category_name' => $category->getName(),
                'publications' => $pubRepo->findLatestInCategory($category, 0, 3, "createdAt", "DESC")));
        }
        return $this->render('contentViewer/homepage.html.twig',
            array('pubs_by_cat' => $pubsByCategory->toArray(),
                  'categories'=>$categories));
    }
}
