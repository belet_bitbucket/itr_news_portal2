<?php
/**
 * Created by PhpStorm.
 * User: Антон
 * Date: 03.05.2016
 * Time: 12:46
 */
namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\Type\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction()
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render(
            'security/login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $lastUsername,
                'error'         => $error,
            )
        );
    }

    /**
     * @Route("/register", name="user_registration")
     */
    public function registerAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $em = $this->getDoctrine()->getManager();
            $defaultRole = $em->getRepository('AppBundle:Role')->findOneByRole('ROLE_USER');
            $user->setRole($defaultRole);
            $em->persist($user);
            $em->flush();
            return $this->sendActivationEmail($user);
        }

        return $this->render(
            'security/register/register.html.twig',
            array('form' => $form->createView())
        );
    }
    
    public function sendActivationEmail(User $user)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $token = $em->getRepository('AppBundle:AccessToken')->generateToken($user);
        $message = \Swift_Message::newInstance()
            ->setSubject('Activation account at News-Portal')
            ->setFrom('send@example.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                    'emails/activate.html.twig',
                    array('name' => $user->getUsername(),
                        'link' =>$token->getToken())
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);
        return $this->render('security/register/register_link_sent.html.twig');
    }

    /**
     * @Route("/activate_acc/{token}", name="activate_acc")
     */
    public function restorePassAction($token)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $token = $em->getRepository("AppBundle:AccessToken")->findOneByToken($token);
        if($token) {
            $token->getUser()->setIsActive(true);
            $em->remove($token);
            $em->flush();
            return $this->render('security/register/activation_success.html.twig');
        }
    }
}