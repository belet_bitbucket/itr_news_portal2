<?php


namespace AppBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Created by PhpStorm.
 * User: Антон
 * Date: 12.06.2016
 * Time: 14:52
 */

class SearchController extends Controller
{
    /**
     *@Route ("/search", name="search_publication")
     * @Method("GET")
     */
    public function searchPublicationAction(Request $request)
    {
        $search_text = $request->query->get('search_text');
        $em    = $this->get('doctrine.orm.entity_manager');
        $categories = $em->getRepository('AppBundle:Category')->findAll();
        $qb = $em->createQueryBuilder();
        $query = $qb->select('p')
            ->from('AppBundle:Publication','p')
            ->where('p.title LIKE :searchText')
            ->orWhere('p.description LIKE :searchText')
            ->setParameter('searchText','%'.$search_text.'%')
            ->getQuery();
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/,
            array('defaultSortFieldName' => 'p.createdAt', 'defaultSortDirection' => 'desc')
        );
        return $this->render('contentViewer/category.html.twig',
            array('pagination' => $pagination,
                'categories'=>$categories));

    }
}