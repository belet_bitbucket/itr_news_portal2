<?php
/**
 * Created by PhpStorm.
 * User: Антон
 * Date: 01.06.2016
 * Time: 17:44
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ForgotPasswordController extends Controller
{
    /**
     * @Route("/forgot_password/{token}", name="forgot_password",defaults={"token" =""})
     */
    public function restorePassAction(Request $request, $token)
    {
        if($token === "")
            return $this->renderRestorePage($request);
        else
            return $this->useToken($request,$token);
    }

    private function renderRestorePage(Request $request)
    {
        $errors = new ArrayCollection();
        if($request->getMethod() === "POST" ){
            $em = $this->getDoctrine()->getEntityManager();
            $user = null;
            $username = $request->request->get("username");
            $email = $request->request->get("email");
            if($username !== "") {
                $user = $em->getRepository('AppBundle:User')->findOneByUsername($username);
                if(is_null($user))
                    $errors->add("No user with this username");
            }
            elseif ($email !== "") {
                $user = $em->getRepository('AppBundle:User')->findOneByEmail($email);
                if(is_null($user))
                    $errors->add("No user with this email");
            }
            else
                $errors->add("Please, enter email or username");
            if(count($errors)==0) {
                return $this->restorePassword($user);
            }
            
        }
        return $this->render(
            'security/forgot/forgot.html.twig',
            array(
                'errors' => $errors,
            )
        );
    }

    private function useToken(Request $request,$tokenstr)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $token = $em->getRepository("AppBundle:AccessToken")->findOneByToken($tokenstr);
        if($token)
        {
            if ($request->getMethod() == "POST") {
                $user = $token->getUser();
                if( $request->request->get('password') ==  $request->request->get('repeat_password')) {
                    $password = $this->get('security.password_encoder')
                        ->encodePassword($user, $request->request->get('password'));
                    $user->setPassword($password);
                    $em->remove($token);
                    $em->flush();
                    return $this->render('security/forgot/set_new_pass.html.twig',
                        array(
                            'message' => 'Password successfully restored.',
                            'error'=>''
                        ));
                }
                else
                    return $this->render('security/forgot/set_new_pass.html.twig',
                        array(
                            'error' => "Error! Password and repeat password mismatch.",
                            'message' => ""
                        ));
            }
            return $this->render('security/forgot/set_new_pass.html.twig',array('error'=>"",'message'=>""));
        }
        return $this->redirectToRoute('homepage');
    }

    
    private function restorePassword(User $user)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $token = $em->getRepository('AppBundle:AccessToken')->generateToken($user);
        $message = \Swift_Message::newInstance()
            ->setSubject('Restoring password at News-Portal')
            ->setFrom('send@example.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                    'emails/forgot.html.twig',
                    array('name' => $user->getUsername(),
                            'link' =>$token->getToken())
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);
        return $this->render('security/forgot/forgot_link_sent.html.twig');
    }


}