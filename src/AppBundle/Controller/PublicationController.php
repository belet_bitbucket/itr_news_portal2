<?php
/**
 * Created by PhpStorm.
 * User: Антон
 * Date: 03.06.2016
 * Time: 23:14
 */
namespace AppBundle\Controller;
use AppBundle\Entity\Category;
use AppBundle\Entity\Publication;
use AppBundle\Form\Type\PublicationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session;
class PublicationController extends Controller
{
    /**
     * @Route("/publication/{id}", name="publication")
     */
    public function openPublicationAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $publication = $em->getRepository('AppBundle:Publication')->findOneById($id);
        $publication->setRate($publication->getRate()+1);
        $em->flush();
        return $this->render('publication.html.twig',
            array('publication'=>$publication));
    }

    /**
     * @Route("/management/publications/{category_id}", name="publicationsgrid", defaults={"category_id" = null})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function publicationsGridAction(Request $request,$category_id)
    {
        // replace this example code with whatever you need
        $em    = $this->get('doctrine.orm.entity_manager');
        $categories = $em->getRepository('AppBundle:Category')->findAll();
        $qb = $em->createQueryBuilder();
        $query = $qb->select('p')
            ->from('AppBundle:Publication','p');
        if(!is_null($category_id))
            $query->where("p.category = :category")
                ->setParameter('category', $category_id);
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/,
            array('defaultSortFieldName' => 'p.createdAt', 'defaultSortDirection' => 'desc')
        );
        return $this->render('management/grids/publications_grid.html.twig',
            array('pagination' => $pagination,
                  'categories'=>$categories));
    }

    /**
     * @Route("/management/publications/{id}/edit", name="publicationedit")
     * @Security("has_role('ROLE_MANAGER')")
     */
    public function publicationEditAction(Request $request, $id)
    {
        $em    = $this->get('doctrine.orm.entity_manager');
        $publication = $em->getRepository('AppBundle:Publication')->findOneById($id);
        $form = $this
            ->createForm(PublicationType::class, $publication, ['method' => 'PUT']);
        $deleteForm = $this->createFormBuilder(new \stdClass())
            ->setAction($this->generateUrl('publicationdelete', ['id' => $id]))
            ->setMethod('DELETE')
            ->getForm();
        if ($request->getMethod() == 'PUT') {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em->flush();
                return $this->redirectToRoute('publication',['id'=>$id]);
            }
        }
        return $this->render(
            ':management:create_or_edit.html.twig',
            [
                'form' => $form->createView(),
                'deleteForm' => $deleteForm->createView()
            ]
        );
    }
    /**
     * @Route("/management/publications/{id}/delete", name="publicationdelete")
     * @Security("has_role('ROLE_MANAGER')")
     */
    public function categoryDeleteAction(Request $request, $id)
    {
        $em    = $this->get('doctrine.orm.entity_manager');
        $publication = $em->getRepository('AppBundle:Category')->findOneById($id);
        if($publication)
        {
            $em->remove($publication);
            $em->flush();
        }
        return $this->redirectToRoute('publicationsgrid');
    }

    /**
     * @Route("/management/publication/new", name="publicationcreate")
     * @Security("has_role('ROLE_MANAGER')")
     */
    public function categoryCreateAction(Request $request)
    {
        $em    = $this->get('doctrine.orm.entity_manager');
        $publication = new Publication();
        $form = $this->createForm(PublicationType::class, $publication);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $publication->setAuthor($this->get('security.token_storage')->getToken()->getUser());
            if ($form->isSubmitted() && $form->isValid()) {
                $em->persist($publication);
                $em->flush();
                return $this->redirectToRoute('publication',['id'=>$publication->getId()]);
            }
        }
        return $this->render(
            ':management:create_or_edit.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }
}