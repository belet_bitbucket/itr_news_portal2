<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Form\Type\CategoryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends Controller
{
    /**
     * @Route("/category/{name}", name="category")
     */
    public function openCategoryAction(Request $request,$name)
    {
        $em    = $this->get('doctrine.orm.entity_manager');
        $categories = $em->getRepository('AppBundle:Category')->findAll();
        $targetCategory = $em->getRepository('AppBundle:Category')->findOneByName($name);
        $dql   = "SELECT p FROM AppBundle:Publication p WHERE p.category = ".$targetCategory->getId();
        $query = $em->createQuery($dql);
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/,
            array('defaultSortFieldName' => 'p.createdAt', 'defaultSortDirection' => 'desc')
        );
        return $this->render('contentViewer/category.html.twig',
            array('pagination' => $pagination,
                  'categories'=>$categories));
    }

    /**
     * @Route("/management/categories", name="categorygrid")
     * @Security("has_role('ROLE_MANAGER')")
     */
    public function categoryGridAction(Request $request)
    {
        // replace this example code with whatever you need
        $em    = $this->get('doctrine.orm.entity_manager');
        $qb = $em->createQueryBuilder();
        $query = $qb->select('c')
            ->from('AppBundle:Category','c')
            ->getQuery();
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );
        return $this->render('management/grids/categories_grid.html.twig',
            array('pagination' => $pagination));
    }
    /**
     * @Route("/management/categories/{id}/edit", name="categoryedit")
     * @Security("has_role('ROLE_MANAGER')")
     */
    public function categoryEditAction(Request $request, $id)
    {
        $em    = $this->get('doctrine.orm.entity_manager');
        $category = $em->getRepository('AppBundle:Category')->findOneById($id);
        $form = $this
            ->createForm(CategoryType::class, $category, ['method' => 'PUT']);
        $deleteForm = $this->createFormBuilder(new \stdClass())
            ->setAction($this->generateUrl('categorydelete', ['id' => $id]))
            ->setMethod('DELETE')
            ->getForm();
        if ($request->getMethod() == 'PUT') {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em->flush();
                return $this->redirectToRoute('categorygrid');
            }
        }
        return $this->render(
            ':management:create_or_edit.html.twig',
            [
                'form' => $form->createView(),
                'deleteForm' => $deleteForm->createView()
            ]
        );
    }
    /**
     * @Route("/management/categories/{id}/delete", name="categorydelete")
     * @Security("has_role('ROLE_MANAGER')")
     */
    public function categoryDeleteAction(Request $request, $id)
    {
        $em    = $this->get('doctrine.orm.entity_manager');
        $category = $em->getRepository('AppBundle:Category')->findOneById($id);
        if($category)
        {
            $em->remove($category);
            $em->flush();
        }
        return $this->redirectToRoute('categorygrid');
    }

    /**
     * @Route("/management/categories/new", name="categorycreate")
     * @Security("has_role('ROLE_MANAGER')")
     */
    public function categoryCreateAction(Request $request)
    {
        $em    = $this->get('doctrine.orm.entity_manager');
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em->persist($category);
                $em->flush();
                return $this->redirectToRoute('categorygrid');
            }
        }
        return $this->render(
            ':management:create_or_edit.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }
}
