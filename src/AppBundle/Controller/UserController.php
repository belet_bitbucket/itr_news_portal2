<?php
/**
 * Created by PhpStorm.
 * User: Антон
 * Date: 06.06.2016
 * Time: 10:20
 */
namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\Type\UserEditType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    /**
     * @Route("/management/users", name="usersgrid")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function usersGridAction(Request $request)
    {
        // replace this example code with whatever you need
        $filter = new User();
        $form = $this->createFormBuilder($filter)
            ->add('email',TextType::class)
            ->add('username',TextType::class)
            ->add('filter',SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        $em    = $this->get('doctrine.orm.entity_manager');
        $qb = $em->createQueryBuilder();
        $query = $qb->select('u')
            ->from('AppBundle:User','u')
            ->where('u.email LIKE :emailFilter')
            ->andWhere('u.username LIKE :usernameFilter')
            ->setParameter('emailFilter',"%".(is_null($filter->getEmail())?"":$filter->getEmail())."%")
            ->setParameter('usernameFilter',"%".(is_null($filter->getUsername())?"":$filter->getUsername())."%")
            ->getQuery();
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );
        return $this->render('management/grids/users_grid.html.twig',
            array('pagination' => $pagination,
                'form' => $form->createView()));
    }



    /**
     * @Route("/management/users/{id}/edit", name="useredit")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function categoryEditAction(Request $request, $id)
    {
        $em    = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('AppBundle:User')->findOneById($id);
        $user->setPlainPassword(" ");
        $form = $this
            ->createForm(UserEditType::class, $user, ['method' => 'PUT']);
        if ($request->getMethod() == 'PUT') {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em->flush();
                return $this->redirectToRoute('usersgrid');
            }
        }
        return $this->render(
            ':management:create_or_edit.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }
}
