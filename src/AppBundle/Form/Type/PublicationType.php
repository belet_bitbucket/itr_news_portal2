<?php
namespace AppBundle\Form\Type;
use Doctrine\ORM\EntityRepository;
use PUGX\AutocompleterBundle\Form\Type\AutocompleteType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class PublicationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('category',EntityType::class,array(
                'class' => 'AppBundle\Entity\Category',
                'choice_label'=>'name'
            ))
            ->add('description', TextareaType::class)
            ->add('body',TextareaType::class)
            ->add('createdAt',DateTimeType::class)
            ->add('similarPublications', EntityType::class,
                [
                    'class' => 'AppBundle\Entity\Publication',
                    'choice_label' => 'title',
                    'multiple' => true,
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                            ->orderBy('p.title', 'ASC');
                    },
                ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Publication',
        ));
    }
}